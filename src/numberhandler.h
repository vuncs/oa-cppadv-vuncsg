#pragma once

#include <functional>

template<typename T, typename CONTAINER>
class NumberHandler
{
public:
    void fill(int count, std::function<T()> generator);
    CONTAINER select(std::function<bool(T)> filter);
    const CONTAINER& list();

private:
    CONTAINER container;
};

template<typename T, typename CONTAINER>
void NumberHandler<T, CONTAINER>::fill(int count, std::function<T()> generator)
{
    for (int i = 0; i < count; i++)
    {
        container.push_back(generator());
    }
}

template<typename T, typename CONTAINER>
CONTAINER NumberHandler<T, CONTAINER>::select(std::function<bool(T)> filter)
{
    CONTAINER p;

    std::for_each(container.begin(), container.end(), [&] (auto i) 
        {
            if (filter(i))
            {
                p.push_back(i);
            }
        } );
    return p;         
}

template<typename T, typename CONTAINER>
const CONTAINER& NumberHandler<T, CONTAINER>::list()
{
    return container;
}


