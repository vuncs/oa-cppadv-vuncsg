#pragma once

#include <random>
#include <functional>

class Util
{
public:
    template<typename T>
    static T random(T min, T max);

    template<typename T>
    static bool isPrime(T number);

    static std::string formatList(auto container);
};

template<typename T>
T Util::random(T min, T max)
{
    std::random_device generator;
	std::uniform_int_distribution<T> distribution(min, max);

    return distribution(generator);
}

template<typename T>
bool Util::isPrime(T number)
{
    bool result = true;

    for(int i = 2; i <= number / 2; ++i)
    {
        if(number % i == 0)
        {
            result = false;
            break;
        }
    }

    return result;
}


std::string Util::formatList(auto container)
{
    std::string s = "";
    std::for_each(container.begin(), container.end(), [&] (auto i) {return s += std::to_string(i) + " "; } );
    s += '\n';
    return s;
}
