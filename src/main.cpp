#include <vector>
#include <iostream>
#include "numberhandler.h"
#include "util.h"

typedef int Number;

int main(int argc, char *argv[])
{
	NumberHandler<Number, std::vector<Number>> numbers;
	auto randomNumber = std::bind(Util::random<Number>, 1, 20);
	numbers.fill(100, randomNumber);
	std::cout << "Numbers: " << Util::formatList(numbers.list());
	std::cout << "Primes: " << Util::formatList(numbers.select(Util::isPrime<Number>));
	return 0;
}
